package app.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Apache Commons Validator package has methods for
 * various validations: https://commons.apache.org/proper/commons-validator/
 */

public class Car {

    /**
     * A local unique identifier
     */
    private UUID carID = UUID.randomUUID();

    /**
     * North America VIN standard 1981
     */
    private Vin vin;

    /**
     * Brand of the car (e.g. Audi)
     */
    @NotNull
    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9-_]+$")
    @Size(max = 124)
    private String make;

    /**
     * A name used by manufacture to market a range
     * of similar cars (e.g. Q5)
     */
    @NotNull
    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9-_]+$")
    @Size(max = 124)
    private String model;

    /**
     * Vehicle Registration Number Sensitive and unique.
     */
    private Vrn vrn;

    /**
     * Photo of the car.
     * private File photo;
     */

    public Car(String vin, String make, String model, char[] vrn) {
        if (vrn == null) { throw new IllegalArgumentException("VRN can't be null"); }

        this.vin = new Vin(vin);
        this.make = make;
        this.model = model;
        this.vrn = new Vrn(vrn.clone());
    }

    public String getCarID() {
        return this.carID.toString();
    }

    public Vin getVin() {
        return this.vin;
    }

    public String getMake() {
        return this.make;
    }

    public String getModel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " "  + this.vin + " " + this.make + " " + this.model;
    }

}
