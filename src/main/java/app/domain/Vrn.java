package app.domain;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

final public class Vrn {

    private final char[] vrn;

    private AtomicBoolean consumed = new AtomicBoolean(false);

    public Vrn(char[] vrn) {
        if (vrn.length == 0) {
            throw new IllegalArgumentException("VRN can't be empty");
        }

        this.vrn = vrn.clone();
    }

    public char[] getVrn() {
        if (consumed.get()) {
            throw new IllegalStateException("Vrn was already consumed");
        }
        consumed.set(true);
        char[] tmpVrn = vrn.clone();
        Arrays.fill(this.vrn, '0');

        return tmpVrn;
    }

    @Override
    public String toString() {
        return "VRN";
    }
}
