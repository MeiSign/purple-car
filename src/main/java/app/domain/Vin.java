package app.domain;

import java.util.HashMap;
import java.util.Map;

public class Vin {

    private final Map<String, Integer> vinKeys = initializeVinKeys();
    private final Map<Integer, Integer> vinWeights = initializeVinWeights();

    private final String vin;

    public Vin(String vin) {
        if (vin == null) throw new IllegalArgumentException("Vin can't be null");
        if (vin.length() != 17) throw new IllegalArgumentException("Invalid length for vin");
        if (!vin.matches("^[ABCDEFGHJKLMNPRXYZ0-9]*$")) throw new IllegalArgumentException("Invalid charactor for vin");
        if (!isValidVin(vin)) throw new IllegalArgumentException("Vin checkdigit validation failed");

        this.vin = vin;
    }

    private boolean isValidVin(String vin) {
        char[] chars = vin.toCharArray();
        Integer sum = 0;

        for (int i = 0; i < chars.length; i++) {
            if (Character.isDigit(chars[i])) {
                sum += Integer.parseInt(String.valueOf(chars[i])) * vinWeights.get(i);
            } else {
                sum += vinKeys.get(String.valueOf(chars[i])) * vinWeights.get(i);
            }
        }


        Integer checkDigit = (Character.isDigit(chars[8])) ? Integer.parseInt(String.valueOf(chars[8])) : 10;

        return (sum) % 11 == checkDigit;
    }

    private Map<String, Integer> initializeVinKeys() {
        Map<String, Integer> vinKeys = new HashMap<>();
        vinKeys.put("A", 1);
        vinKeys.put("B", 2);
        vinKeys.put("C", 3);
        vinKeys.put("D", 4);
        vinKeys.put("E", 5);
        vinKeys.put("F", 6);
        vinKeys.put("G", 7);
        vinKeys.put("J", 1);
        vinKeys.put("K", 2);
        vinKeys.put("L", 3);
        vinKeys.put("M", 4);
        vinKeys.put("N", 5);
        vinKeys.put("P", 7);
        vinKeys.put("R", 9);
        vinKeys.put("S", 2);
        vinKeys.put("T", 3);
        vinKeys.put("U", 4);
        vinKeys.put("V", 5);
        vinKeys.put("W", 6);
        vinKeys.put("X", 7);
        vinKeys.put("Y", 8);
        vinKeys.put("Z", 9);

        return vinKeys;
    }

    private Map<Integer, Integer> initializeVinWeights() {
        Map<Integer, Integer> vinWeights = new HashMap<>();
        vinWeights.put(0, 8);
        vinWeights.put(1, 7);
        vinWeights.put(2, 6);
        vinWeights.put(3, 5);
        vinWeights.put(4, 4);
        vinWeights.put(5, 3);
        vinWeights.put(6, 2);
        vinWeights.put(7, 10);
        vinWeights.put(8, 0);
        vinWeights.put(9, 9);
        vinWeights.put(10, 8);
        vinWeights.put(11, 7);
        vinWeights.put(12, 6);
        vinWeights.put(13, 5);
        vinWeights.put(14, 4);
        vinWeights.put(15, 3);
        vinWeights.put(16, 2);

        return vinWeights;
    }

    @Override
    public String toString() {
        return "Vin(" + vin + ")";
    }
}
