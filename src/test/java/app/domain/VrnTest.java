package app.domain;

import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


class VrnTest {

    @Test
    public void shouldOnlyBeConsumableOnce() {
        Vrn vrn = new Vrn("CharArray".toCharArray());
        assertThat(vrn.getVrn(), is("CharArray".toCharArray()));
        assertThrows(IllegalStateException.class, vrn::getVrn);
    }
}
