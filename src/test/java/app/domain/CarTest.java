package app.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import static org.hamcrest.core.Is.is;

import javax.validation.Valid;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@WebMvcTest
class CarTest {

    @Autowired
    private LocalValidatorFactoryBean validator;

    private final String VALID_VIN = "1M8GDM9AXKP042788";

    @Test
    public void allFieldsShouldBeNonNullable() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Car(null, "make", "model", "vrn".toCharArray());
        });

        assertThrows(IllegalArgumentException.class, () -> {
            new Car(VALID_VIN, "make", "model", null);
        });

        Car car1 = new Car(VALID_VIN, null, "model", "vrn".toCharArray());
        assertThat(validator.validate(car1).isEmpty(), is(false));


        Car car2 = new Car(VALID_VIN, "make", null, "vrn".toCharArray());
        assertThat(validator.validate(car2).isEmpty(), is(false));
    }

    @Test
    public void makeAndModelShouldOnlyAcceptValidCharacters() {
        Car car1 = new Car(VALID_VIN, "make", "@#@#@#", "vrn".toCharArray());
        assertThat(validator.validate(car1).isEmpty(), is(false));

        Car car2 = new Car(VALID_VIN, "@#@#", "model", "vrn".toCharArray());
        assertThat(validator.validate(car2).isEmpty(), is(false));
    }

    @Test
    public void allFieldsShouldNotAcceptEmptyValues() {
        assertThrows(IllegalArgumentException.class, () -> {
           new Car("", "make", "model", "vrn".toCharArray());
        });

        assertThrows(IllegalArgumentException.class, () -> {
            new Car(VALID_VIN, "make", "model", "".toCharArray());
        });

        Car car1 = new Car(VALID_VIN, "", "model", "vrn".toCharArray());
        assertThat(validator.validate(car1).isEmpty(), is(false));


        Car car2 = new Car(VALID_VIN, "make", "", "vrn".toCharArray());
        assertThat(validator.validate(car2).isEmpty(), is(false));
    }

    @Test
    public void makeAndModelShouldNotHaveMoreThan124Characters() {
        Car car1 = new Car(VALID_VIN, "make", "tooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLong", "vrn".toCharArray());
        assertThat(validator.validate(car1).isEmpty(), is(false));

        Car car2 = new Car(VALID_VIN, "tooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLongtooLongTooLong", "model", "vrn".toCharArray());
        assertThat(validator.validate(car2).isEmpty(), is(false));
    }

}
