package app.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VinTest {

    @Test
    public void vinShouldNotBeNull() {
        assertThrows(IllegalArgumentException.class, () -> new Vin(null));
    }

    @Test
    public void vinShouldHaveASizeOf17Characters() {
        assertThrows(IllegalArgumentException.class, () -> new Vin(""));
        assertThrows(IllegalArgumentException.class, () -> new Vin("A"));
        assertThrows(IllegalArgumentException.class, () -> new Vin("AAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
    }

    @Test
    public void vinShouldNotContainIQLChars() {
        assertThrows(IllegalArgumentException.class, () -> new Vin("AAA1AAAAAAAAAAAAI"));
        assertThrows(IllegalArgumentException.class, () -> new Vin("AAAAAAAAAAAAAAAAQ"));
        assertThrows(IllegalArgumentException.class, () -> new Vin("AAAAAAAAAAAAAAAAO"));
    }

    @Test
    public void vinShouldBeCheckedByCheckdigit() {
        new Vin("1M8GDM9AXKP042788");
        assertThrows(IllegalArgumentException.class, () -> new Vin("3M8GDM9AXKP042788"));

    }

}
