= Purple Car

//tag::abstract[]

You are part of the development team at Purple Car, an online car sale company
(nobody should ever walk). 
You will be given objectives during the course and
use this program as a base.

Remember to send a *pull* request once you completed
all the labs.

//end::abstract[]

//tag::lab[]

== Build and run

*Fork* and clone *your forked repository*.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: Tests are incomplete.

//end::lab[]

//tag::references[]

//end::references[]
